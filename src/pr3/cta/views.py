from django.shortcuts import render
from .models import Cta
# Create your views here.

def index_views(request):    
    posts = Cta.objects.all()
    return render(request, 'mysite/index.html', {'posts': posts})